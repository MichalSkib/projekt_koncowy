from flask import Flask, render_template, request
from flask_sqlalchemy import SQLAlchemy
from zipfile import ZipFile

app = Flask(__name__)

@app.route("/", methods=["GET", "POST"])
def hello_world():
    context = {
        "start_year": request.form.get('start_year'),
        "end_year": request.form.get('end_year')
    }
    return render_template("web.html", context=context)

app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///test.db' #test db to nazwa pliku w którym utworzymy bazę danych
db = SQLAlchemy(app)


class Country(db.Model):
  id = db.Column(db.Integer, primary_key=True)
  username = db.Column(db.String(80), unique=True, nullable=False)
  email = db.Column(db.String(120), unique=True, nullable=False)
  firstname = db.Column(db.String(120), unique=False, nullable=False)
  lastname = db.Column(db.String(120), unique=False, nullable=False, server_default="", default="")


zip_data = "https://https://databank.worldbank.org/AjaxDownload/FileDownloadHandler.ashx?filename=e2b8ac4b-a80f-455b-9e57-22f816e3456f.zip&filetype=CSV&language=en&displayfile=Data_Extract_From_Sustainable_Development_Goals_(SDGs).zip"

print(zip_data)
with ZipFile(zip_data) as myzip:
    with myzip.open('eggs.txt') as myfile:
        print(myfile.read())

1. Utworzyć klasę bazy danych
2. otworzyć plik csv
